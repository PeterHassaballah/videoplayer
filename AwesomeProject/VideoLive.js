import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet ,TouchableOpacity,Text} from 'react-native'
import Video from 'react-native-video'

export default class VideoComponent extends React.Component {
    state = {
        rate: 1,
        volume: 1,
        muted: false,
        resizeMode: 'contain',
        duration: 0.0,
        currentTime: 0.0,
        paused: true,
      };

    renderRateControl(rate) {
        const isSelected = (this.state.rate === rate);
    
        return (
          <TouchableOpacity onPress={() => { this.setState({ rate }) }}>
            <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
              {rate}x
            </Text>
          </TouchableOpacity>
        );
      }

      renderVolumeControl(volume) {
        const isSelected = (this.state.volume === volume);
    
        return (
          <TouchableOpacity onPress={() => { this.setState({ volume }) }}>
            <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
              {volume * 100}%
            </Text>
          </TouchableOpacity>
        )
      }

      renderResizeModeControl(resizeMode) {
        const isSelected = (this.state.resizeMode === resizeMode);
    
        return (
          <TouchableOpacity onPress={() => { this.setState({ resizeMode }) }}>
            <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
              {resizeMode}
            </Text>
          </TouchableOpacity>
        )
      }

  renderVideo () {
      return(
        <Video
          source={require('./broadchurch.mp4')}
        // source={{uri :' https://www.youtube.com/watch?v=QPzrrGKg_Wk' , type: 'mpd'} }
          style={{ width: 800, height: 800 }}
          muted={false}
          repeat={false}
          resizeMode={"contain"}
          volume={1.0}
          rate={1.0}
        //   ignoreSilentSwitch={"obey"}

        />
      )
  }

  render () {
    return (
      <View>
          <View style={{width: 280, height: 140}}>
        {this.renderVideo()}
        </View>
        <View style={styles.controls}>
            <View style={styles.generalControls}>
              <View style={styles.rateControl}>
                {this.renderRateControl(0.25)}
                {this.renderRateControl(0.5)}
                {this.renderRateControl(1.0)}
              </View>

              <View style={styles.volumeControl}>
                {this.renderVolumeControl(0.5)}
                {this.renderVolumeControl(1)}
              </View>
              <View style={styles.resizeModeControl}>
                {this.renderResizeModeControl('cover')}
                {this.renderResizeModeControl('contain')}
                {this.renderResizeModeControl('stretch')}
              </View>
              
  
            </View>
        </View>
       
        {/* <View style={styles.trackingControls}>
              <View style={styles.progress}>
                <View style={[styles.innerProgressCompleted, { flex: flexCompleted }]} />
                <View style={[styles.innerProgressRemaining, { flex: flexRemaining }]} />
              </View>
            </View> */}
      
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'black',
    },
  
    fullScreen: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },
    controls: {
      backgroundColor: 'transparent',
      borderRadius: 5,
      position: 'absolute',
      bottom: 20,
      left: 20,
      right: 20,
    },
    progress: {
      flex: 1,
      flexDirection: 'row',
      borderRadius: 3,
      overflow: 'hidden',
    },
    innerProgressCompleted: {
      height: 20,
      backgroundColor: '#cccccc',
    },
    innerProgressRemaining: {
      height: 20,
      backgroundColor: '#2C2C2C',
    },
    generalControls: {
      flex: 1,
      flexDirection: 'row',
      borderRadius: 4,
      overflow: 'hidden',
      paddingBottom: 10,
    },
    rateControl: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
    },
    volumeControl: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
    },
    resizeModeControl: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    },
    controlOption: {
      alignSelf: 'center',
      fontSize: 11,
      color: 'white',
      paddingLeft: 2,
      paddingRight: 2,
      lineHeight: 12,
    },
  });